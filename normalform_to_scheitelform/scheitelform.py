def plusorminus(z):
    return "-" if z<0 else "+"


def quadratisch_ergaenzen(a,b,c):

    b=b/a
    c=c/a
    print()
    print("Ausklammern:")
    print(f'f(x)= {float(a)} * [x² {plusorminus(b)} {abs(b)}*x  {plusorminus(c)} {abs(c)}]',end='\n\n')
    
    
    print("quadratisch Ergänzen:")
    print(f'f(x)= {float(a)} * [x² {plusorminus(b)} {abs(b)}*x + ({b}/2)² - ({b}/2)² {plusorminus(c)} {abs(c)}]',end='\n\n')
    
    print("binomische Formel anwenden")
    
    
    print(f'f(x)= {float(a)} * [(x {plusorminus(b)} {abs(b/2)})²  - ({b}/2)² {plusorminus(c)} {abs(c)})]',end='\n\n')
    
    b=(b/2)
    c=(-(b**2)+c)*a

    print('Scheitelform:')
    print(f'f(x)= {float(a)} * (x {plusorminus(b)} {abs(b)})²  {plusorminus(c)} {abs(c)}',end='\n\n')

    print(f'S = ({-b} , {c})')

#########################################


# Bsp.: f(x) = -x² + 2x -3
# a = -1 ; b = 2 ; c = -3

quadratisch_ergaenzen(-1, 2, -3)
